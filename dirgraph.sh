#!/bin/sh
POSIXLY_CORRECT=yes

print_help() {

    help="\nUsage: dirgraph [OPTIONS] [DIR]\nALL arguments are optional!
    \n[OPTIONS]
    \t-h\tprint this help message
    \t-n\tnormalise the histogram
    \t  \t(scales down the histogram to fit the availible row width)
    \t  \t/be aware that after scaling small values might totally disappear from the histogram/
    \t-i\tignores regex inserted right after this parameter
    \t  \t/be aware that regex must contain only name of directory or file -- NOT a path/
    \n[DIR]
    \tThe directory that one wants to scan.
    \t/be aware, that if not specified takes current working directory/
    \t/be aware, that directories, which user doesn't have permission to open will be skipped/
    "
    echo "$help"
}



# Arguments parsing
ignore=0
IGNORE_DIR=""
NORMALISE=0
help=0

while getopts "hi:n" c; do
  case $c in
    i) IGNORE_DIR=$OPTARG
       ignore=1
       shift
       shift;;
    n) NORMALISE=1 
       shift;;
    h) help=1
       shift;;
  esac
done

if [ "$help" -eq 1 ]; then
    print_help
    exit 0
fi
export ignore
export IGNORE_DIR

# set DIR from args if found
if [ ! -z "$2" ]; then
    echo "Invalid arguments!" 1>&2
    exit 1
fi
if [ -z "$@" ]; then 
    DIR=`pwd`
else
    if [ -d "$@" ]; then
        DIR=$@
    else
        echo "Wrong directory!" 1>&2
        exit 1
    fi
fi

# check if $IGNORE_DIR is valid 
if [ $ignore -eq 1 ]; then
    for i in `echo "$DIR" | tr '/' '\n'`
    do
        #if [ ! -z `echo "$i" | grep "$IGNORE_DIR"` ]; then
        if [ "$i" = "$IGNORE_DIR" ]; then
            echo "Invalid -i argument!"
            exit 1
        fi
    done
fi 

files=0
directories=0

case_one=0
case_two=0
case_three=0
case_four=0
case_five=0
case_six=0
case_seven=0
case_eight=0
case_nine=0

# parsing sizes into variables
parse_size(){
    if [ "$1" -le 100 ]; then
        case_one=`expr $case_one + 1`
    elif [ "$1" -le 1024 ]; then
        case_two=`expr $case_two + 1`
    elif [ "$1" -le 10240 ]; then
        case_three=`expr $case_three + 1`
    elif [ "$1" -le 102400 ]; then
        case_four=`expr $case_four + 1`
    elif [ "$1" -le 1048576 ]; then
        case_five=`expr $case_five + 1`
    elif [ "$1" -le 10485760 ]; then
        case_six=`expr $case_six + 1`
    elif [ "$1" -le 104857600 ]; then
        case_seven=`expr $case_seven + 1`
    elif [ "$1" -le 1073742000 ]; then
        case_eight=`expr $case_eight + 1`
    else
        case_nine=`expr $case_nine + 1`
    fi
}

run_scan() {
    find "$DIR" 2>/dev/null | { 
        while IFS= read -r file; do

            ignore_this=0
            
            # if IGNORE_DIR found in any part of current file path set ignore_this to 1
            if [ "$ignore" -eq 1 ]; then
                for i in `echo $file | tr '/' '\n'`
                do
                    #if [ ! -z `echo "$i" | grep "$IGNORE_DIR"` ]; then
                    if [ "$i" = "$IGNORE_DIR" ]; then
                        ignore_this=1
                        break
                    fi
                done
            fi
            
            if [ -f "$file" ] && [ $ignore_this -eq 0 ]; then

                size=`wc -c "$file" 2>/dev/null | awk '{print $1}'`
                files=`expr $files + 1`
                if [ ! -z "$size" ]; then
                    parse_size $size;
                fi
            elif [ -d "$file" ] && [ $ignore_this -eq 0 ]; then 
                directories=`expr $directories + 1`;
            fi

        done;
        
        # needed to get values to parent shell using eval after function call
        echo "files=$files"
        echo "directories=$directories"
        echo "case_one=$case_one"
        echo "case_two=$case_two"
        echo "case_three=$case_three"
        echo "case_four=$case_four"
        echo "case_five=$case_five"
        echo "case_six=$case_six"
        echo "case_seven=$case_seven"
        echo "case_eight=$case_eight"
        echo "case_nine=$case_nine"
    }
}

scan_results=`run_scan`
eval $scan_results

#determine columns value based on if output is terminal
if [ -t 1 ] ; then
    columns=`tput cols`
    columns=`expr $columns - 11`
else
    columns=69
fi

export columns

normalise() {
    #find which case was highest file count
    max_case=0
    for case_x in $@; do
        if [ "$case_x" -gt "$max_case" ]; then
            max_case="$case_x"
        fi
    done
   
    #get scale only if needed (if terminal is sufficient and user specifies -n do nothing)
    if [ "$columns" -gt "$max_case" ]; then
        return
    fi
   
    scale=`awk -v max_case="$max_case" -v columns="$columns" -v scale=0 'BEGIN{ scale=columns/max_case
                                                                                printf scale
                                                                              }'`

    #actual change of values - scaling to fit terminal
    index=1
    for case_x in $@; do
        case_variables="case_one,case_two,case_three,case_four,case_five,case_six,case_seven,case_eight,case_nine" 
        printf "$case_variables" | awk -v case_x="$case_x" -v scale="$scale" -v i="$index" -F',' '{printf $i}
                                                                                                  END{case_x = case_x * scale
                                                                                                     printf "=%.0f\n",case_x}'
        index=`expr $index + 1`
    done 
   
}

if [ "$NORMALISE" -eq 1 ]; then
    normalised=`normalise $case_one $case_two $case_three $case_four $case_five $case_six $case_seven $case_eight $case_nine`
    eval $normalised
fi

print_graph() {
    echo "File size histogram"
    index=1
    for case_x in $@; do
        export case_x
        beginings="<100 B  : ,<1 KiB  : ,<10 KiB : ,<100 KiB: ,<1 MiB  : ,<10 MiB : ,<100 MiB: ,<1 GiB  : ,>=1GiB  : "
        printf "$beginings" | awk -v x="$case_x" -v i="$index" -F',' '{printf $i}
                                                                       END{for(c=0;c<x;c++) printf "#"; printf "\n"}'
        index=`expr $index + 1`
    done
}

#output section
echo "Root directory: $DIR"
echo "Directories: $directories"
echo "All files: $files"
print_graph $case_one $case_two $case_three $case_four $case_five $case_six $case_seven $case_eight $case_nine
